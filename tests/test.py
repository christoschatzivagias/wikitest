import math
import requests

import argparse
import random



args = {}
args['host'] = "localhost"
args['port'] = "8080"


def test_pages_count():
   req = requests.get("http://" + args['host'] + ":" + args["port"])
   print(req.content)
   assert req.content == b'OK'

   homereq = requests.get("http://" + args['host'] + ":" + args["port"] + "/api/pages").json()

   assert len(homereq) >= 3


def test_home_page():
   req = requests.get("http://" + args['host'] + ":" + args["port"])
   print(req.content)
   assert req.content == b'OK'

   homereq = requests.get("http://" + args['host'] + ":" + args["port"] + "/api/page/home").json()

   assert homereq['url'] == 'home' 


# def test_get_sort_history_request():
#    originalstrlist = "999,666,333"
#    sortedstrlist = "333,666,999"
#    data = '{"original":"'+ originalstrlist +'"}'
#    requests.post("http://" + args['host'] + ":" + args["port"] + "/api/numberarrays", data=data, headers = {'Content-type': 'application/json'})
#    req = requests.get("http://" + args['host'] + ":" + args["port"] + "/api/numberarrays", headers = {'Content-type': 'application/json'}).json()
#    assert(req['content'][-1]['original'] == originalstrlist)
#    assert(req['content'][-1]['sorted'] == sortedstrlist)
