var express = require('express');
var app = express();
var Sequelize = require("sequelize");
var morgan = require('morgan');
var bodyParser = require('body-parser');
var multer = require('multer');

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    next();
});

app.use(morgan('dev'));
app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

var storage = multer.diskStorage({
      destination: function (req, file, cb) {
      cb(null, 'public/img')
    },
    filename: function (req, file, cb) {
      cb(null,file.originalname )
    }
})

var upload = multer({ storage: storage }).single('file')

const connection = new Sequelize('db', 'user', 'pass', {
  host: 'localhost',
  dialect: 'sqlite',
  storage: 'db.sqlite',
  operatorsAliases: false
})

var Page = connection.define('Pages', {
  url: {
       type: Sequelize.STRING,
       primaryKey: true
  },
  title: Sequelize.STRING,
  text: Sequelize.TEXT
})

var History = connection.define('History', {
  id: {
       type: Sequelize.INTEGER,
       primaryKey: true
  },
  url: Sequelize.STRING,
  title: Sequelize.STRING,
  reason: Sequelize.STRING,
  newText: Sequelize.TEXT,
  oldText: Sequelize.TEXT,
})

connection.sync({
  logging: console.log
})
.then(() => {
  console.log('Connection to database established successfully.');
})
.catch(err => {
  console.log('Unable to connect to the database: ', err);
})

app.get('/', (req, res) => {

	Page.findOne({where: {url: 'home'}}).then(async (page) => {
    if (page == null) {
       var page = await Page.create({ url: "home"});
       page.title = "Welcome to nodewiki ";
       page.text = "```Installation```.\r\n========\r\nYou must have\r\n* nodejs\r\n* mongodb\r\n\r\n"
       + "Clone the repository, and inside the project directory, do\r\n\r\n    $ npm install\r\n\r\n"
       + "Running the code\r\n=============\r\nJust do\r\n\r\n    $ npm start\r\n\r\n"
       + "and the wiki should start. You can navigate to localhost:8080, and you should be greeted by "
       + "the home page.\r\n\r\nFeatures\r\n=======\r\n* MathJax integration\r\n* basic image uploading\r\n"
       + "* markdown support via marker\r\n* on the go creation of new pages\r\n* pages searchable\r\n\r\n"
       + "To Do\r\n=====\r\n* more theming\r\n* image uploading without losing edit progress\r\n* image search\r\n"
       + "* all searches via dropdown\r\n* deleting pages\r\n\r\n![Simple Desktops](/img/sunRising.png)";
       page.save();

       page = await Page.create({ url: "home2"});
       page.title = "Welcome to nodewiki2 ";
       page.text = "```Installation```.\r\n========\r\nYou must have\r\n* nodejs\r\n* mongodb\r\n\r\n"
       + "Clone the repository, and inside the project directory, do\r\n\r\n    $ npm install\r\n\r\n"
       + "Running the code\r\n=============\r\nJust do\r\n\r\n    $ npm start\r\n\r\n"
       + "and the wiki should start. You can navigate to localhost:8080, and you should be greeted by "
       + "the home page.\r\n\r\nFeatures\r\n=======\r\n* MathJax integration\r\n* basic image uploading\r\n"
       + "* markdown support via marker\r\n* on the go creation of new pages\r\n* pages searchable\r\n\r\n"
       + "To Do\r\n=====\r\n* more theming\r\n* image uploading without losing edit progress\r\n* image search\r\n"
       + "* all searches via dropdown\r\n* deleting pages\r\n\r\n![Simple Desktops](/img/sunRising.png)";
       page.save();


       page = await Page.create({ url: "home3"});
       page.title = "Welcome to nodewiki3 ";
       page.text = "```Installation```.\r\n========\r\nYou must have\r\n* nodejs\r\n* mongodb\r\n\r\n"
       + "Clone the repository, and inside the project directory, do\r\n\r\n    $ npm install\r\n\r\n"
       + "Running the code\r\n=============\r\nJust do\r\n\r\n    $ npm start\r\n\r\n"
       + "and the wiki should start. You can navigate to localhost:8080, and you should be greeted by "
       + "the home page.\r\n\r\nFeatures\r\n=======\r\n* MathJax integration\r\n* basic image uploading\r\n"
       + "* markdown support via marker\r\n* on the go creation of new pages\r\n* pages searchable\r\n\r\n"
       + "To Do\r\n=====\r\n* more theming\r\n* image uploading without losing edit progress\r\n* image search\r\n"
       + "* all searches via dropdown\r\n* deleting pages\r\n\r\n![Simple Desktops](/img/sunRising.png)";
       page.save();
    }
    res.status(200).send("OK");
  }).catch(error => {
    console.log(error);
    res.status(404).send(error);
  })

});

app.post('/api/new', (req, res) => {
  Page.create({
      url: (new Buffer(req.body.title)).toString('base64'),
      title: req.body.title,
      text: req.body.text
    }
  ).then(function (pages) {
        if (pages) {
            res.send(pages);
        } else {
            res.status(400).send('Error in create new page');
        }
    });
});

app.get('/api/pages', function(req, res) {

  Page.findAll({attributes: ['url', 'title', 'updatedAt']}).then(pages => {
    res.status(200).send(pages);
  }).catch(error => {
    console.log(error);
    res.status(404).send(error);
  })

});

app.get('/api/historylist/:url', function(req, res) {

  History.findAll(
    {
      where:{
          url: req.params.url
        },
  }).then(histories => {
    res.status(200).send(histories);
  }).catch(error => {
    console.log(error);
    res.status(404).send(error);
  })

});

const findLinks = (text) => {
  const regexMdLinks = /\[([^\[]+)\](\(.*\))/gm
  const matches = text.match(regexMdLinks)
  const singleMatch = /\[([^\[]+)\]\((.*)\)/
  if(matches == null)
    return []
  return matches.map(match => singleMatch.exec(match)[2].substring(1))
}

app.get('/api/page/:url', function(req, res) {

  Page.findOne({where:{url: req.params.url}}).then(page => {
    const links = findLinks(page.dataValues.text);
    Page.findAll({where:{url: links}}).then(pages => {
      page.dataValues.validLinks = pages.map(page=>page.dataValues.url)
      res.status(200).send(page);
    })
  }).catch(error => {
    console.log(error);
    res.status(404).send(error);
  })

});

app.post('/api/upload',function(req, res) {

    upload(req, res, function (err) {
           if (err) {
               return res.status(500).json(err)
           }
      return res.status(200).send(req.file)
    })

});

app.post('/api/search/:query', function(req, res) {

  const queryList = req.params.query.split('+')

  const query = queryList.map(query=>{
    return {
      [Sequelize.Op.or]:{
        text: {
            [Sequelize.Op.like]: '%' + query + '%'
        },
        title: {
            [Sequelize.Op.like]: '%' + query + '%'
        }
      }
    }
    
  })

	Page.findAll({
    where: {
        [Sequelize.Op.or]:query
      }
    }).then(pages => {
      res.status(200).send(pages);
    }).catch(error => {
      console.log(error);
      res.status(404).send(error);
    });
});

app.get('/api/history/:id',  (req, res) => {

  History.findByPk(req.params.id).then((history) => {
    res.status(200).send(history)
  })

})

app.post('/api/save/:url', (req, res) => {

  Page.findOne({where:{url: req.params.url}}).then(async (page) => {
    if (page != null) {
      var history = await History.create(
        {
          url: req.params.url,
          title: page.title,
          reason: req.body.reason,
          oldText: page.text,
          newText: req.body.text
        }
      );
      history.save();

      page.title = req.body.title;
      page.text = req.body.text;
      page.save();

    } else {
      var newpage = await Page.create(
        {
          title: req.body.title,
          text: req.body.text
        }
      );
      newpage.save()
    }
    res.status(200).send("OK");

  }).catch(error => {
    console.log(error);
    res.status(404).send(error);
  });

});

app.listen(8080);
console.log('Listening...');
