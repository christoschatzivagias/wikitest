import React, { Component } from "react";
import { Nav, Navbar, Button } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { withRouter } from "react-router-dom";
import SearchBar from "./SearchBar";

class NavComp extends Component {
  handleSearchTextChange(event) {
    this.setState({
      searchText: event.target.value,
    });

    this.props.handleSearchTextChange(event);
  }

  handleSearchClick = () => {
    console.log(this.props.history);
    this.props.handleSearchClick();
  };

  render() {
    return (
      <Navbar bg="light" expand="lg">
        <Navbar.Brand href="/">WIKI</Navbar.Brand>
        <Navbar.Brand href="/new">
          <Button>New</Button>
        </Navbar.Brand>
        <Navbar.Brand href="/upload">
          <Button>Upload</Button>
        </Navbar.Brand>
        <Navbar.Collapse className="justify-content-end">
          <Nav pullright="true">
            <SearchBar />
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

export default withRouter(NavComp);
