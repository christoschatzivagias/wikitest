import React, { Component } from "react";
import { Container, Col, Row } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { getPages } from "./UtilityFunctions";
import dateformat from "dateformat";


class Main extends Component {
  constructor() {
    super();
    this.state = {
      pages: [],
    };
  }

  componentDidMount() {
    getPages().then((pages) => {
      this.setState({
        pages: pages,
      });
    });
  }

  render() {
    const { pages } = this.state;
    const pageLinks = pages.map((page) => (
      <Row key={page.url}>
        <Col>
          <h3>
            <a href={"/view/" + page.url}>{page.title}</a>
          </h3>
        </Col>
        <Col>
          <div className="float-right">
            <i>
              Updated At {dateformat(new Date(page.updatedAt))}
            </i>
          </div>
        </Col>
      </Row>
    ));
    return (
      <Container fluid>
        <h2>Page List</h2>
        {pageLinks}
      </Container>
    );
  }
}

export default Main;
