import React, { Component } from "react";
import { Container, Button, Row, Col } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { postNewPage } from "./UtilityFunctions";
import ServerMessage from './ServerMessage'
import MarkdownIt from "markdown-it";
import MdEditor from "react-markdown-editor-lite";
// import style manually
import "react-markdown-editor-lite/lib/index.css";
import "./extra.css";

const mdParser = new MarkdownIt(/* Markdown-it options */);

class New extends Component {
  constructor() {
    super();
    this.state = {
      title: "",
      text: "",
      status: 0
    };
  }

  createNewPage() {
    postNewPage(
      this.state.title,
      this.state.text
    ).then(status => {
      console.log("New page:" + status)
      this.setState({
        status: status
      })
    })

  }

  handleEditorChange({ html, text }) {
    this.setState({
      text: text,
    });
  }

  titleChange(event) {
    this.setState({
      title: event.target.value,
    });
  }

  render() {
    const { title, text, status} = this.state;
    return (
      <Container fluid>
        <Row>
          <Col xs={10}>
            <Container fluid>
              <Row>
                <Col>
                  <h3>Title:</h3>
                </Col>
                <Col xs={10}>
                  <input
                    style={{ minWidth: "100%" }}
                    type="text"
                    value={title}
                    onChange={this.titleChange.bind(this)}
                  />
                </Col>
              </Row>
            </Container>
          </Col>
          <Col>
            <Button
              style={{margin: "2px"}}
              onClick={this.createNewPage.bind(this)}
            >
              Save
            </Button>
            <Button
              style={{margin: "2px"}}
              variant="light"
              href={"/"}>Cancel</Button>
          </Col>
        </Row>

        <MdEditor
          style={{height:"500px"}}
          value={text}
          renderHTML={(text) => {
            return mdParser.render(text);
          }}
          onChange={this.handleEditorChange.bind(this)}
        />
        <ServerMessage show={status === 200} redirect={'/'} title='Success!' message='Page created successfully!'/>
        <ServerMessage show={status === 404} redirect={'/'} title='Fail!' message='Unable to create page, server error 404!'/>

      </Container>
    );
  }
}

export default New;
