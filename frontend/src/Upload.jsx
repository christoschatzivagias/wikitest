import React, { Component } from "react";
import  { Redirect } from 'react-router-dom'
import { Container, Form, Row, Col, Button } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { uploadFile } from "./UtilityFunctions";
import ServerMessage from './ServerMessage'

class Upload extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedFile: "",
      uploadDisable: true,
      status: 0,
      redirect: false
    };
  }

  onFileChangeHandler(event) {
    const uploadDisable =
      event.target.files[0] === null || this.state.name === "";

    this.setState({
      selectedFile: event.target.files[0],
      uploadDisable: uploadDisable,
    });
  }

  removeMessage() {
    this.setState({
      redirect:true
    })
  }

  onClickHandler() {
    const data = new FormData();
    data.append("file", this.state.selectedFile);
    uploadFile(data).then(status => {
      this.setState({
        status: status
      })

      setTimeout(this.removeMessage.bind(this), 2000);

    });

  }

  render() {
    const { uploadDisable, status, redirect, selectedFile } = this.state;
    if(redirect)
      return (<Redirect to='/' />)
    return (
      <Container fluid>
        <Row>
          <Col>
            <Form.File.Input onChange={this.onFileChangeHandler.bind(this)} />
            <Button
              disabled={uploadDisable}
              onClick={this.onClickHandler.bind(this)}
            >
              Upload
            </Button>
          </Col>
        </Row>

        <ServerMessage show={status === 200} redirect={'/'} title='Success!' message={'Image uploaded successfully' + selectedFile.name}/>
        <ServerMessage show={status === 404} redirect={'/'} title='Fail!' message='Unable to upload image, server error 404!'/>

      </Container>
    );
  }
}

export default Upload;
