import React, { Component } from "react";
import { Container, Table } from "react-bootstrap";
import { getHistoryList } from "./UtilityFunctions";
import dateformat from "dateformat";

import "bootstrap/dist/css/bootstrap.min.css";
import "./extra.css";
import PageNav from "./PageNav";

class HistoryList extends Component {
  constructor() {
    super();
    this.state = {
      histories: [],
    };
  }

  componentWillMount() {
    getHistoryList(this.props.match.params.url).then((histories) => {
      this.setState({
        histories: histories,
      });
    });
  }

  render() {
    const url = this.props.match.params.url;
    return (
      <Container fluid>
        <PageNav url={url} />
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Change Description</th>
              <th>Edited At</th>
              <th>Diff</th>
            </tr>
          </thead>
          <tbody>
            {this.state.histories.map((history) => (
              <tr key={history.id}>
                <td>{history.reason}</td>
                <td>{dateformat(new Date(history.updatedAt))}</td>
                <td>
                  <a href={"/history/" + history.id}>Diff</a>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </Container>
    );
  }
}

export default HistoryList;
