import React, { Component } from "react";
import { Nav } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

class PageNav extends Component {
  render() {
    return (
      <Nav className="float-right">
        <Nav.Item>
          <Nav.Link href={"/view/" + this.props.url}>View</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link href={"/edit/" + this.props.url}>Edit</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link href={"/historylist/" + this.props.url}>History</Nav.Link>
        </Nav.Item>
      </Nav>
    );
  }
}

export default PageNav;
