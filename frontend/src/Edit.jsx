import React, { Component } from "react";
import { Container, Button, Row, Col } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { getPage, postPage } from "./UtilityFunctions";
import MarkdownIt from "markdown-it";
import MdEditor from "react-markdown-editor-lite";
// import style manually
import "react-markdown-editor-lite/lib/index.css";
import "./extra.css";
import PageNav from "./PageNav";
import ServerMessage from './ServerMessage'


const mdParser = new MarkdownIt(/* Markdown-it options */);

class Edit extends Component {
  constructor() {
    super();
    this.state = {
      title: "",
      text: "",
      changeReason: "",
    };
  }

  componentWillMount() {
    getPage(this.props.match.params.url).then((page) => {
      this.setState({
        title: page.title,
        text: page.text,
        status: 0
      });
    });
  }

  handleEditorChange({ html, text }) {
    this.setState({
      text: text,
    });
  }

  reasonChange(event) {
    this.setState({
      reason: event.target.value,
    });
  }

  titleChange(event) {
    this.setState({
      title: event.target.value,
    });
  }

  savePage() {
    postPage(
      this.props.match.params.url,
      this.state.reason,
      this.state.title,
      this.state.text
    ).then(status => {
      this.setState({
        status: status
      })
    })

  }

  render() {
    const { title, text, status } = this.state;
    const url = this.props.match.params.url;
    return (
      <Container fluid>
        <PageNav url={url} />
        <Row>
          <Col xs={10}>
            <Container fluid>
              <Row>
                <Col>
                  <h3>Title:</h3>
                </Col>
                <Col xs={8}>
                  <input
                    style={{ minWidth: "100%" }}
                    type="text"
                    value={title}
                    onChange={this.titleChange.bind(this)}
                  />
                </Col>
              </Row>
              <Row>
                <Col>
                  <h3>Describe the change:</h3>
                </Col>
                <Col xs={8}>
                  <input
                    style={{ minWidth: "100%" }}
                    type="text"
                    onChange={this.reasonChange.bind(this)}
                  />
                </Col>
              </Row>
            </Container>
          </Col>
          <Col>
            <Button
              onClick={this.savePage.bind(this)}
            >
              Save
            </Button>
            <Button
             variant="light"
             href={"/view/" + url}>
             Cancel
             </Button>
          </Col>
        </Row>

        <MdEditor
          value={text}
          renderHTML={(text) => {
            return mdParser.render(text);
          }}
          onChange={this.handleEditorChange.bind(this)}
        />
        <ServerMessage show={status === 200} redirect={"/view/" + url} title='Success!' message='Page saved successfully!'/>
        <ServerMessage show={status === 404} redirect={'/'} title='Fail!' message='Unable to create page, server error 404!'/>
      </Container>
    );
  }
}

export default Edit;
