import React, { Component } from "react";
import { Container } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { getHistoryById } from "./UtilityFunctions";
import "./extra.css";
import ReactDiffViewer from "react-diff-viewer";
import PageNav from "./PageNav";

class History extends Component {
  constructor() {
    super();
    this.state = {
      history: {
        url: "",
        oldText: "",
        newText: "",
      },
    };
  }

  componentWillMount() {
    getHistoryById(this.props.match.params.id).then((history) => {
      this.setState({
        history: history,
      });
    });
  }

  render() {
    const { url, oldText, newText, createdAt } = this.state.history;
    return (
      <Container fluid>
        <PageNav url={url} />
        <h3>Edit at {createdAt}</h3>
        <ReactDiffViewer
          oldValue={oldText}
          newValue={newText}
          splitView={true}
        />
      </Container>
    );
  }
}

export default History;
