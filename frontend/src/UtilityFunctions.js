import axios from "axios";
import { service, port } from "../package.json";

const endpoint = service + ":" + port + "/api/";

export const getPage = async (url) => {
  return axios
    .get(endpoint + "page/" + url)
    .then((response) => {
      return response.data;
    })
    .catch((err) => {
      console.error(err.message);
      throw err;
    });
};

export const getPages = async () => {
  return axios
    .get(endpoint + "pages")
    .then((response) => {
      return response.data;
    })
    .catch((err) => {
      console.error(err.message);
      throw err;
    });
};

export const getHistoryList = async (url) => {
  return axios
    .get(endpoint + "historylist/" + url)
    .then((response) => {
      return response.data;
    })
    .catch((err) => {
      console.error(err.message);
      throw err;
    });
};

export const getHistoryById = async (id) => {
  return axios
    .get(endpoint + "history/" + id)
    .then((response) => {
      return response.data;
    })
    .catch((err) => {
      console.error(err.message);
      throw err;
    });
};

export const uploadFile = async (formdata) => {
  return axios
    .post(endpoint + "upload", formdata)
    .then((response) => {
      return response.status;
    })
    .catch((err) => {
      console.error(err.message);
      throw err;
    });
};

export const postSearchQuery = async (query) => {
  return axios
    .post(endpoint + "search/" + query)
    .then((response) => {
      return response.data;
    })
    .catch((err) => {
      console.error(err.message);
      throw err;
    });
};

export const postPage = async (url, reason, title, text) => {
  return axios
    .post(endpoint + "save/" + url, {
      title: title,
      text: text,
      reason: reason,
    })
    .then((response) => {
      return response.status;
    })
    .catch((err) => {
      console.error(err.message);
      throw err;
    });
};


export const postNewPage = async (title, text) => {
  return axios
    .post(endpoint + "new", {
      title: title,
      text: text,
    })
    .then((response) => {
      return response.status;
    })
    .catch((err) => {
      console.error(err.message);
      throw err;
    });
};


export const convertImageUri = (uri) => {
  return service + ":" + port + uri;
};
