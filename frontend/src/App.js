import React, { Component } from "react";
import "./App.css";
import Main from "./Main";
import Page from "./Page";
import Edit from "./Edit";
import New from "./New";
import HistoryList from "./HistoryList";
import History from "./History";
import Upload from "./Upload";
import SearchList from "./SearchList";
import NavComp from "./NavComp";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <NavComp />
          <Switch>
            <Route exact path="/" component={Main} />
            <Route exact path="/new" component={New} />
            <Route exact path="/search/:query" component={SearchList} />
            <Route exact path="/view/:url" component={Page} />
            <Route exact path="/edit/:url" component={Edit} />
            <Route exact path="/historylist/:url" component={HistoryList} />
            <Route exact path="/history/:id" component={History} />
            <Route exact path="/upload" component={Upload} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
