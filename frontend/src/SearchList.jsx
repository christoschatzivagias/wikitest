import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";
import { postSearchQuery } from "./UtilityFunctions";
import dateformat from "dateformat";
import "bootstrap/dist/css/bootstrap.min.css";
import "./extra.css";

class SearchList extends Component {
  constructor() {
    super();
    this.state = {
      searchResults: [],
    };
  }

  componentDidMount() {
    postSearchQuery(this.props.match.params.query).then((results) => {
      this.setState({
        searchResults: results,
      });
    });
  }

  render() {
    const { searchResults } = this.state;
    return (
      <Container fluid>
        <h1>Search Results:</h1>
        {
          searchResults.map((result) => (
            <Row>
              <Col>
                <h3>
                  <a href={"/view/" + result.url}>{result.title}</a>
                </h3>
              </Col>
              <Col>
                <div className="float-right">
                  <i>
                    Updated At {dateformat(new Date(result.updatedAt))}
                  </i>
                </div>
              </Col>
            </Row>
          ))
        }
      </Container>
    );
  }
}

export default SearchList;
