import React, { Component } from "react";
import { Container, Badge } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "./extra.css";

import { getPage, convertImageUri } from "./UtilityFunctions";
import ReactMarkdown from "react-markdown";
import PageNav from "./PageNav";

class Page extends Component {
  constructor() {
    super();
    this.state = {
      title: "",
      text: "",
      validLinks: [],
    };
  }

  componentWillMount() {
    getPage(this.props.match.params.url).then((page) => {
        this.setState({
          title: page.title,
          text: page.text,
          validLinks: page.validLinks
        });
    });
  }

  render() {
    const { title, text, validLinks } = this.state;
    const url = this.props.match.params.url;
    if (title === undefined && text === undefined) {
      return (
        <span>
          <h3>
            <Badge variant="warning">Page {url} does not exist</Badge>
          </h3>
        </span>
      );
    }

    return (
      <Container fluid>
        <PageNav url={url} />
        <h1>{title}</h1>
        <ReactMarkdown
          source={text}
          linkTarget={(url, text, title) => {
            return !validLinks.includes(url.substring(1)) ? "_broken_" : undefined;
          }}
          escapeHtml={false}
          transformImageUri={(uri) => convertImageUri(uri)}
        />
      </Container>
    );
  }
}

export default Page;
